#include "message.h"

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

static char buffer[1024];

static int dowrite(bot_t *aBot, char *buffer, int rc) {
    if (aBot->wlog) aBot->wlog(aBot, buffer, rc);
    return write(aBot->fd, buffer, rc);
}

int write_join_msg(bot_t *aBot)
{
    int rc;
    rc = snprintf(buffer, sizeof(buffer),
            "{\"msgType\":\"join\",\"data\":{\"name\":\"%s\",\"key\":\"%s\"}}\n", aBot->name, aBot->key);
    return dowrite(aBot, buffer, rc);
}

int write_createrace_msg(bot_t *aBot, char *trackname, char *password, int carcount)
{
    int rc;

    if (password) {
        rc = snprintf(buffer, sizeof(buffer),
                "{\"msgType\":\"createRace\",\"data\":{\"trackName\":\"%s\",\"password\":\"%s\",\"carCount\":%d,\"botId\":{\"name\":\"%s\",\"key\":\"%s\"}}}\n",
                trackname,
                password,
                carcount,
                aBot->name,
                aBot->key);
    } else {
        rc = snprintf(buffer, sizeof(buffer),
                "{\"msgType\":\"createRace\",\"data\":{\"trackName\":\"%s\",\"carCount\":%d,\"botId\":{\"name\":\"%s\",\"key\":\"%s\"}}}\n",
                trackname,
                carcount,
                aBot->name,
                aBot->key);
    }

    return dowrite(aBot, buffer, rc);
}

int write_joinrace_msg(bot_t *aBot, char *trackname, char *password, int carcount)
{
    int rc;

    if (password) {
        rc = snprintf(buffer, sizeof(buffer),
                "{\"msgType\":\"joinRace\",\"data\":{\"trackName\":\"%s\",\"password\":\"%s\",\"carCount\":%d,\"botId\":{\"name\":\"%s\",\"key\":\"%s\"}}}\n",
                trackname,
                password,
                carcount,
                aBot->name,
                aBot->key);
    } else {
        rc = snprintf(buffer, sizeof(buffer),
                "{\"msgType\":\"joinRace\",\"data\":{\"trackName\":\"%s\",\"carCount\":%d,\"botId\":{\"name\":\"%s\",\"key\":\"%s\"}}}\n",
                trackname,
                carcount,
                aBot->name,
                aBot->key);
    }

    return dowrite(aBot, buffer, rc);
}

int write_ping_msg(bot_t *aBot)
{
    int rc;

    rc = snprintf(buffer, sizeof(buffer), "{\"msgType\":\"ping\",\"data\":\"ping\"}\n");

    return dowrite(aBot, buffer, rc);
}

int write_throttle_msg(bot_t *aBot, double throttle)
{
    int rc;

    if (throttle > 1.0) throttle = 1.0;
    else if (throttle < 0.0) throttle = 0.1;

    rc = snprintf(buffer, sizeof(buffer), 
            "{\"msgType\":\"throttle\",\"data\":%lf}\n",
            throttle);

    return dowrite(aBot, buffer, rc);
}

int write_switch_msg(bot_t *aBot, int left)
{
    int rc;

    if (left)
        rc = snprintf(buffer, sizeof(buffer), "{\"msgType\":\"switchLane\",\"data\":\"Left\"}\n");
    else
        rc = snprintf(buffer, sizeof(buffer), "{\"msgType\":\"switchLane\",\"data\":\"Right\"}\n");

    return dowrite(aBot, buffer, rc);
}

int write_turbo_msg(bot_t *aBot)
{
    int rc;

    rc = snprintf(buffer, sizeof(buffer), 
            "{\"msgType\":\"turbo\",\"data\":\"go turbo!\"}\n");

    return dowrite(aBot, buffer, rc);
}
