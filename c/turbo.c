#include "turbo.h"

int turboParse(cJSON *aData, turbo_t *aTurbo)
{
    cJSON *obj;

    aTurbo->m_turbodurationmilliseconds = 0;
    aTurbo->m_turbofactor = 0;
    aTurbo->m_turbodurationticks = 0;

    obj = cJSON_GetObjectItem(aData, "turboDurationMilliseconds");
    if (obj)
        aTurbo->m_turbodurationmilliseconds = obj->valuedouble;

    obj = cJSON_GetObjectItem(aData, "turboFactor");
    if (obj)
        aTurbo->m_turbofactor = obj->valuedouble;

    obj = cJSON_GetObjectItem(aData, "turboDurationTicks");
    if (obj)
        aTurbo->m_turbodurationticks = obj->valueint;

    return 0;
}
