#ifndef __CALC_H_INC__
#define __CALC_H_INC__

#include "race.h"

extern double getPieceSize(lane_t *aLanes, piece_t *aPiece, int aStart, int aEnd);

#endif /* __CALC_H_INC__ */
