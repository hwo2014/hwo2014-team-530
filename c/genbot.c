#include "abot.h"

#include "message.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

typedef struct
{
    bot_t m_bot;
    double m_throttle;
} GenBot_t;

#if 0
static int extractBotInfo(cJSON *aData, char *aName, int aNameSz, char *aColor, int aColorSz)
{
    cJSON *obj;

    obj = cJSON_GetObjectItem(aData, "name");
    if (!obj) return -1;
    strncpy(aName, obj->valuestring, aNameSz);

    obj = cJSON_GetObjectItem(aData, "color");
    if (!obj) return -1;
    strncpy(aColor, obj->valuestring, aColorSz);

    return 0;
}
#endif

static void log_message(char *msg_type_name, cJSON *msg)
{
#if 0
    cJSON *msg_data;
    char name[32] = {};
    char color[32] = {};

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("spawn", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data)
            extractBotInfo(msg_data, name, sizeof(name), color, sizeof(color));

        printf("Someone spawn (%s - %s\n)", name, color);
    } else if (!strcmp("crash", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data)
            extractBotInfo(msg_data, name, sizeof(name), color, sizeof(color));
        printf("Someone crashed (%s - %s\n)", name, color);
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
#endif
}

int GenBot_process_msg(struct _bot *aBot, cJSON *aMsg)
{
    GenBot_t *_bot = (GenBot_t *) aBot;
    cJSON *msg_type;
    char *msg_type_name;

    msg_type = cJSON_GetObjectItem(aMsg, "msgType");
    msg_type_name = msg_type->valuestring;

    if (!strcmp("carPositions", msg_type_name)) {
        write_throttle_msg(aBot, _bot->m_throttle);
    } else {
        log_message(msg_type_name, aMsg);
    }

    return 0;
}

int GenBot_wlog(struct _bot *aBot, const char *aBuffer, int aSize)
{
    return 0;
}

int GenBot_rlog(struct _bot *aBot, const char *aBuffer, int aSize)
{
    return 0;
}

int GenBot_close(struct _bot *aBot)
{
    GenBot_t *_bot = (GenBot_t *) aBot;

    free(_bot);

    return 0;
}

void GenBotInit(bot_t **aBot, int fd, const char *aName, const char *aKey, int aSilent)
{
    GenBot_t *abot;

    abot = calloc(1, sizeof(*abot));

    *aBot = &abot->m_bot;

    abot->m_bot.fd = fd;
    abot->m_throttle = ((rand() % 60)+41) / 100.0;

    strncpy(abot->m_bot.name, aName, sizeof(abot->m_bot.name));
    strncpy(abot->m_bot.key, aKey, sizeof(abot->m_bot.key));

    abot->m_bot.process_msg = &GenBot_process_msg;
    abot->m_bot.rlog = &GenBot_rlog;
    abot->m_bot.wlog = &GenBot_wlog;
    abot->m_bot.close = &GenBot_close;
}
