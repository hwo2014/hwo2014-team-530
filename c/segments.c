#include "segments.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "curves.h"

void calcSwitchCosts(lane_t *aLanes, int aPiece, int aCurrent, int aNumLanes, double *aCostL, double *aCostR)
{
    piece_t *ptr = &aLanes[aCurrent].m_pieces[aPiece];

    if (!ptr->m_isswitch) {
        return;
    }

    if (ptr->m_isarc) {
        double radius = ptr->m_radius;

        if (ptr->m_angle < 0)
            radius -= aLanes[aCurrent].m_distancefromcenter;
        else
            radius += aLanes[aCurrent].m_distancefromcenter;

        // right
        if (aCurrent - 1 >= 0) {
            *aCostR =  
                bendCrossCurveLength(
                    aLanes[aCurrent].m_distancefromcenter, 
                    aLanes[aCurrent-1].m_distancefromcenter, 
                    radius,
                    ptr->m_angle) - ptr->m_length;
        }

        // left
        if (aCurrent + 1 < aNumLanes) {
            *aCostL = 
                bendCrossCurveLength(
                    aLanes[aCurrent].m_distancefromcenter, 
                    aLanes[aCurrent+1].m_distancefromcenter, 
                    radius,
                    ptr->m_angle) - ptr->m_length;
        }
    }

    double plen = ptr->m_length;

    // left
    if (aCurrent - 1 >= 0) {
        double ldiff = fabs(aLanes[aCurrent-1].m_distancefromcenter) + fabs(aLanes[aCurrent].m_distancefromcenter);

        if ((aLanes[aCurrent-1].m_distancefromcenter < 0 && aLanes[aCurrent].m_distancefromcenter < 0) ||
            (aLanes[aCurrent-1].m_distancefromcenter > 0 && aLanes[aCurrent].m_distancefromcenter > 0))
        {
            ldiff = fabs(aLanes[aCurrent-1].m_distancefromcenter - aLanes[aCurrent].m_distancefromcenter);
        }

        *aCostL = sqrt(plen * plen + ldiff*ldiff);
    }

    // right
    if (aCurrent + 1 < aNumLanes) {
        double ldiff = fabs(aLanes[aCurrent+1].m_distancefromcenter) + fabs(aLanes[aCurrent].m_distancefromcenter);

        if ((aLanes[aCurrent+1].m_distancefromcenter < 0 && aLanes[aCurrent].m_distancefromcenter < 0) ||
            (aLanes[aCurrent+1].m_distancefromcenter > 0 && aLanes[aCurrent].m_distancefromcenter > 0))
        {
            ldiff = fabs(aLanes[aCurrent+1].m_distancefromcenter - aLanes[aCurrent].m_distancefromcenter);
        }

        *aCostR = sqrt(plen * plen + ldiff*ldiff);
    }
}

int makeSegments(segments_t *aSegments, int aNumLanes, lane_t *aLanes)
{
    int numswitches = 0;
    int ic;
    int *switchpieces;
    int jc;

    if (!aNumLanes)
        return 0;

    for (ic = 0; ic < aLanes->m_numpieces; ic++) {
        if (aLanes->m_pieces[ic].m_isswitch) {
            numswitches++;
        }
    }

    if (numswitches == 0)
        return 0;

    switchpieces = calloc(numswitches, sizeof(*switchpieces));

    fprintf(stdout, "switches ");
    for (jc = 0, ic = 0; ic < aLanes->m_numpieces; ic++) {
        if (aLanes->m_pieces[ic].m_isswitch) {
            fprintf(stdout, " %d", ic);
            switchpieces[jc] = ic;
            jc++;
        }
    }
    fprintf(stdout, "\n");

    aSegments->m_segments = calloc(numswitches, sizeof(*aSegments->m_segments));
    aSegments->m_numsegments = numswitches;
    aSegments->m_numlanes = aNumLanes;
    for (ic = 0; ic < aSegments->m_numsegments; ic++) {
        aSegments->m_segments[ic].m_lengthperlane =
            calloc(aSegments->m_numlanes, sizeof(*(aSegments->m_segments[ic].m_lengthperlane)));
    }

    // first segment always have to be inversed
    {
        aSegments->m_segments[0].m_startpiece = switchpieces[numswitches-1] + 1;
        aSegments->m_segments[0].m_endpiece   = switchpieces[0] - 1;
        aSegments->m_segments[0].m_flip = 1;

        if (aSegments->m_segments[0].m_endpiece < 0)
            aSegments->m_segments[0].m_endpiece = aLanes->m_numpieces - 1;

        if (aSegments->m_segments[0].m_startpiece >= aLanes->m_numpieces)
            aSegments->m_segments[0].m_startpiece = aLanes->m_numpieces - 1;

        for (jc = 0; jc < aNumLanes; jc++) {
            double len;
            for (ic = switchpieces[numswitches-1]+1; ic < aLanes->m_numpieces; ic++) {
#if 0
                piece_t *piece_ptr = &aLanes[jc].m_pieces[ic];

                if (piece_ptr->m_isarc) {
                    fprintf(stdout, "sg: %d(%d): r: %.8lf l: %.8lf\n", ic, piece_ptr->m_lane, piece_ptr->m_radius, piece_ptr->m_length);
                }
#endif

                len = aLanes[jc].m_pieces[ic].m_length;
                aSegments->m_segments[0].m_lengthperlane[jc].m_length += len;
            }

            for (ic = 0; ic <= switchpieces[0]; ic++) {
#if 0
                piece_t *piece_ptr = &aLanes[jc].m_pieces[ic];

                if (piece_ptr->m_isarc) {
                    fprintf(stdout, "sg: %d(%d): r: %.8lf l: %.8lf\n", ic, piece_ptr->m_lane, piece_ptr->m_radius, piece_ptr->m_length);
                }
#endif
                len = aLanes[jc].m_pieces[ic].m_length;
                if (ic == switchpieces[0]) {
                    calcSwitchCosts(aLanes, ic, jc, aNumLanes, 
                                &aSegments->m_segments[0].m_lengthperlane[jc].m_costl,
                                &aSegments->m_segments[0].m_lengthperlane[jc].m_costr);
                }
                aSegments->m_segments[0].m_lengthperlane[jc].m_length += len; 
            }
        }
    }

    for (ic = 0; ic < numswitches - 1; ic++) {
        int start;
        int end;

        start = switchpieces[ic];
        end = switchpieces[ic+1];

        aSegments->m_segments[ic + 1].m_startpiece = start + 1;
        aSegments->m_segments[ic + 1].m_endpiece = end - 1;

        for (jc = 0; jc < aNumLanes; jc++) {
            int kc;
            for (kc = start+1; kc <= switchpieces[ic+1]; kc++) {
                double len = aLanes[jc].m_pieces[kc].m_length;

#if 0
                piece_t *piece_ptr = &aLanes[jc].m_pieces[kc];

                if (piece_ptr->m_isarc) {
                    fprintf(stdout, "sg: %d(%d): r: %.8lf l: %.8lf\n", ic, piece_ptr->m_lane, piece_ptr->m_radius, piece_ptr->m_length);
                }
#endif
                if (kc == switchpieces[ic+1]) {
                }
                aSegments->m_segments[ic + 1].m_lengthperlane[jc].m_length += len; 
            }
        }
    }

    for (ic = 0; ic < aSegments->m_numsegments; ic++)
    {
        double best = 100000;
        int blane = 0;
        fprintf(stdout, "s: %3d e: %3d", 
            aSegments->m_segments[ic].m_startpiece,
            aSegments->m_segments[ic].m_endpiece);
        for (jc = 0; jc < aSegments->m_numlanes; jc++)
        {
            if (aSegments->m_segments[ic].m_lengthperlane[jc].m_length < best) {
                best = aSegments->m_segments[ic].m_lengthperlane[jc].m_length;
                blane = jc;
            }

            fprintf(stdout, " %d: %.8lf", 
                jc,
                aSegments->m_segments[ic].m_lengthperlane[jc].m_length);
        }
        fprintf(stdout, " b: %d\n", blane);
    }

    return 0;
}

int freeSegments(segments_t *aSegments)
{
    int ic;

    for (ic = 0; ic < aSegments->m_numsegments; ic++)
    {
        if (aSegments->m_segments[ic].m_lengthperlane)
            free(aSegments->m_segments[ic].m_lengthperlane);
        aSegments->m_segments[ic].m_lengthperlane = NULL;
    }

    aSegments->m_numsegments = 0;
    free(aSegments->m_segments);
    aSegments->m_segments = NULL;

    return 0;
}
