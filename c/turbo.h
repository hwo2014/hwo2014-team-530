#ifndef __TURBO_H_INC__
#define __TURBO_H_INC__

#include "cJSON.h"

typedef struct {
    int    m_turbodurationticks;
    double m_turbodurationmilliseconds;
    double m_turbofactor;
} turbo_t;

extern int turboParse(cJSON *aData, turbo_t *aTurbo);

#endif /* __TURBO_H_INC__ */
