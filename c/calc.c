#include "calc.h"
#include "curves.h"

#include <math.h>
#include <stdio.h>

//------------------------------------------------------------------------------
double getPieceSize(lane_t *aLanes, piece_t *aPiece, int aStart, int aEnd) 
{
    if (aStart == aEnd) {
        return aPiece->m_length;
    }

    if (!aPiece->m_isarc)
    {
        double ldiff = fabs(aLanes[aStart].m_distancefromcenter) + fabs(aLanes[aEnd].m_distancefromcenter);
        double len = 0;
       
        if ((aLanes[aStart].m_distancefromcenter >= 0.0 && aLanes[aEnd].m_distancefromcenter >= 0.0) ||
            (aLanes[aStart].m_distancefromcenter <= 0.0 && aLanes[aEnd].m_distancefromcenter <= 0.0))
        {
            ldiff = fabs(aLanes[aStart].m_distancefromcenter - aLanes[aEnd].m_distancefromcenter);
        }

        if (ldiff == 20.0 && 
            aPiece->m_length == 100.0)
            len = 102.060390;

        if (ldiff == 20.0 && 
            aPiece->m_length == 99.0)
            len = 101.0805;

        if (ldiff == 20.0 && 
            aPiece->m_length == 94.0)
            len = 96.104110 + 0.0835;

        if (ldiff == 20.0 && 
            aPiece->m_length == 102.0)
            len = 103.942292 + 0.0786;

        if (len == 0)
        {
            len = sqrt(aPiece->m_length*aPiece->m_length + ldiff*ldiff);
#if 1
            fprintf(stdout, "%s:%d %lF %lf %lf = %lf\n", __FILE__, __LINE__, aLanes[aStart].m_distancefromcenter, aLanes[aEnd].m_distancefromcenter, aPiece->m_length, len);
#endif
        }

        return len;
    }

    {
        double radius = aPiece->m_radius;

        if (aPiece->m_angle < 0)
            radius -= aLanes[aStart].m_distancefromcenter;
        else
            radius += aLanes[aStart].m_distancefromcenter;

        return bendCrossCurveLength(
            aLanes[aStart].m_distancefromcenter, 
            aLanes[aEnd].m_distancefromcenter, 
            radius,
            aPiece->m_angle);
    }
}
//------------------------------------------------------------------------------
