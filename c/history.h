#ifndef __HISTORY_H_INC__
#define __HISTORY_H_INC__

#include "position.h"
#include "race.h"

#define MAX_POSITIONS 30000
typedef struct
{
    char m_color[17];
    botposition_t m_positions[MAX_POSITIONS];
    botposition_t *m_head;
    botposition_t *m_tail;
    botposition_t *m_end;
    botposition_t *m_start;
    int m_crash;
    int m_turbo;
    int m_dnf;
} history_t;

extern history_t *historyFind(const char *aColor, history_t *aHistory, int aCount);
extern void historyAddFlag(history_t *aHistory, int aTick, int aCrash, int aSpawn, int aTurboStart, int aTurboEnd);
extern void historyAdd(history_t *aHistory, int aTick, position_t *aPosition, lane_t *aLanes);
extern botposition_t *historyGetPrev(history_t *aHistory, int aTick);
extern botposition_t *historyGetNext(history_t *aHistory, int aTick);
extern void historyInit(char *aColor, history_t *aHistory);

#endif /* __HISTORY_H_INC__ */
