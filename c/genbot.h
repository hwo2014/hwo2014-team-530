#ifndef __GENBOT_H_INC__
#define __GENBOT_H_INC__

#include "bot.h"

extern void GenBotInit(bot_t **aBot, int fd, const char *aName, const char *aKey, int silent);

#endif /* __GENBOT_H_INC__ */
