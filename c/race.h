#ifndef __RACE_H_INC__
#define __RACE_H_INC__

#include "cJSON.h"

typedef struct {
    double m_length;
    double m_angle;
    double m_radius;
    char   m_lane;
    unsigned char   m_isarc    :1;
    unsigned char   m_isswitch :1;
    unsigned char   m_isbridge :1;
} piece_t;

typedef struct {
    char     m_lane;
    char     m_index;
    double   m_distancefromcenter;
    int      m_numpieces;
    piece_t *m_pieces;
} lane_t;

typedef struct {
    char   m_color[32];
    char   m_name[32];
    double m_length;
    double m_width;
    double m_guideflagposition;
} car_t;

typedef struct {
    int      m_numlanes;
    lane_t  *m_lanes;
    int      m_numcars;
    car_t   *m_cars;
    struct {
        int m_laps;
        int m_maxlaptimems;
        int m_durationms;
        unsigned char m_quickrace:1;
    } m_session;
} race_t;

extern int parseTrackInit(cJSON *aData, race_t *aRace, char *aID, int aIDsz);
extern void freeRace(race_t *aRace);
extern int updateRaceSession(cJSON *aData, race_t *aRace);

#endif /* __RACE_H_INC__ */
