#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/epoll.h>
#include <unistd.h>
#include <libgen.h>
#include <time.h>

#include "bot.h"
#include "abot.h"
#include "genbot.h"
#include "message.h"
#include "cJSON.h"

static int gsEpollFd;
static int gsNumBots = 1;
static int gsNumRunning = 0;

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void doSockBlocking(int fd, int blocking)
{
    int flags = fcntl(fd, F_GETFL, 0);
    if (flags < 0) return;
    flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
    fcntl(fd, F_SETFL, flags);
}

static char buffer[32 * 1024] = {};

static int read_msg(bot_t *aBot) {
    int rc;
    int saved = 0;
    int readsz = sizeof(buffer);
    char *start = buffer;
    char *end;
    char *ptr;

    if (aBot->pending_size) {
        memcpy(buffer, aBot->pending, aBot->pending_size);
        saved = aBot->pending_size;
        readsz -= saved;
        start += saved;
        free(aBot->pending);
        aBot->pending = NULL;
        aBot->pending_size = 0;
    }

    doSockBlocking(aBot->fd, 0);

    rc = read(aBot->fd, start, readsz);

    doSockBlocking(aBot->fd, 1);

    if (rc <= 0) {
        if (gsNumRunning)
            gsNumRunning--;
        close(aBot->fd);
        aBot->fd = -1;
        aBot->close(aBot);

        return 0;
    }

    start = buffer;
    end = buffer + rc + saved;

    while ((ptr = memchr(start, '\n', end - start)) != NULL) {
        cJSON *msg;
        *ptr = 0;
        if (aBot->rlog)
            aBot->rlog(aBot, start, ptr - start);

        msg = cJSON_Parse(start);
        *ptr = '\n';
        if (msg) {
             aBot->process_msg(aBot, msg);
             cJSON_Delete(msg);
        }
        start = ++ptr;
    }

    saved = end - start;
    // save pending
    if (saved) {
        aBot->pending = calloc(saved, sizeof(char));
        aBot->pending_size = saved;
        memcpy(aBot->pending, start, saved);

        return 0;
    }
    
    return 0;
}

int mainLoop()
{
    struct epoll_event events[10];

    while (1) {
        int nr = epoll_wait(gsEpollFd, events, sizeof(events)/sizeof(events[0]), 1000);

        if (nr < 0) {
            error("epoll_wait");
        }

        if (nr > 0) {
            int ic;

            for (ic = 0; ic < nr; ic++) {
                bot_t *bot = (bot_t *)events[ic].data.ptr;
                if (events->events & EPOLLIN) {
                    read_msg(bot);
                }
            }
        }

        if (gsNumRunning == 0) {
            break;
        }
    }

    return 0;
}

static int cbot_main(int argc, char *argv[])
{
    int fd;
    bot_t *bot;
    struct epoll_event event = {};

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    gsEpollFd = epoll_create(10);

    fd = connect_to(argv[1], argv[2]);

    ABotInit(&bot, fd, argv[3], argv[4], 1);
    bot->is_ci = 1;

    write_join_msg(bot);

    event.data.ptr = (void *)bot;
    event.events = EPOLLIN;
    epoll_ctl(gsEpollFd, EPOLL_CTL_ADD, fd, &event);
    gsNumRunning++;

    mainLoop();

    return 0;
}

#define BOTKEY  "C/6lEkOTAZCRhA"
#define BOTNAME "Canuck Turbo"

int main(int argc, char *argv[])
{
    int ic;
    char *ptr;
    char *track;
    char *password = "12345a";
    //char *password = NULL;

    ptr = basename(argv[0]);

    if (strcmp(ptr, "cbot") == 0) {
        return cbot_main(argc, argv);
    }

    if (argc < 3)
        error("Usage: bot host port [trackname carcount]\n");

    srand(time(NULL));

    track = "germany";
    track = "france";
    track = "usa";
    track = "keimola";
    track = "england";
    track = "suzuka";
    track = "elaeintarha";
    track = "imola";
    gsNumBots = 1;

    gsEpollFd = epoll_create(10);

    for (ic = 0; ic < gsNumBots; ic++) {
        int fd;
        bot_t *bot;
        fd = connect_to(argv[1], argv[2]);

        if (fd >= 0) {
            char name[32];
            struct epoll_event event = {};
            int rc;

            if (ic == 0) {
                strcpy(name, BOTNAME);
            } else {
                sprintf(name, "%s%d", BOTNAME, ic);
            }
                
            switch (ic) {
                case 0:
                    ABotInit(&bot, fd, name, BOTKEY, 0);
                    break;
                default:
                    GenBotInit(&bot, fd, name, BOTKEY, 1);
                    break;
            }
        
            if (ic == 0)
                rc = write_createrace_msg(bot, track, password, gsNumBots);
                //rc = write_joinrace_msg(bot, track, "asdf", 8);
            else
                rc = write_joinrace_msg(bot, track, password, gsNumBots);
            if (rc == -1) {
                fprintf(stdout, "[%s]: FAILED\n", name);
                return -1;
            }

            event.data.ptr = (void *)bot;
            event.events = EPOLLIN;
            epoll_ctl(gsEpollFd, EPOLL_CTL_ADD, fd, &event);
            gsNumRunning++;
        }
    }

    mainLoop();

    return 0;
}
