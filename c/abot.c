#include "abot.h"

#include "message.h"
#include "race.h"
#include "position.h"
#include "turbo.h"
#include "segments.h"
#include "history.h"
#include "calc.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

//------------------------------------------------------------------------------
static int extractBotInfo(cJSON *aData, char *aName, int aNameSz, char *aColor, int aColorSz)
{
    cJSON *obj;

    obj = cJSON_GetObjectItem(aData, "name");
    if (!obj) return -1;
    strncpy(aName, obj->valuestring, aNameSz);

    obj = cJSON_GetObjectItem(aData, "color");
    if (!obj) return -1;
    strncpy(aColor, obj->valuestring, aColorSz);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
typedef struct
{
    bot_t  m_bot;

    int        m_init;
    char       m_silent;
    race_t     m_race;
    segments_t m_segments;

    char   m_color[32];
    int    m_switchpending;
    int    m_nextswitchpiece;

    history_t *m_history;
    history_t *m_myhistory;
    int m_numcars;

    int     m_turboavailable;
    turbo_t m_turbo;
    double  m_turbo_active; // includes factor
    int     m_turbo_end;
    double  m_throttle;

    int     m_fastest;

    int m_laststraight;
    struct {
        double m_maxvelocity;
    } *m_checkpieces;

    // constants
    double K;
    double M;
    double Fslip;
} ABot_t;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static double velocity_at_tick(double v0, double H, double K, double M, double T)
{
    return (v0 - (H/K) ) * exp( ( - K * T ) / M ) + ( H / K );
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// v(t) = (v(0) - (h/k) ) * e^ ( ( - k * t ) / m ) + ( h/k )
static double calcVelocityNextTick(double v0, double h, double k, double m, int t) 
{
    return (v0 - (h/k) ) * exp( ( - k * (double) t ) / m ) + ( h/k );
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// d(t) = ( m/k ) * ( v(0) - ( h/k ) ) * ( 1.0 - e^ ( ( -k*t ) / m ) ) + ( h/k ) * t + d(0)
static double calcDistanceNextTick(double v0, double h, double k, double m, int t, double d0) 
{
    return  (m  / k) * (v0 - ( h/ k )) * (1.0 - exp((-k * t) / m)) + (h / k ) * t + d0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_analyze(ABot_t * aABot, int aReanalyze)
{
    int ic;
    lane_t *lptr = aABot->m_race.m_lanes;
    double _fslip = 0.31;

    if (aReanalyze == 0)
    {
        aABot->m_checkpieces = calloc(lptr->m_numpieces, sizeof(*aABot->m_checkpieces));

        aABot->m_laststraight = -1;
        for (ic = lptr->m_numpieces - 1; ic >= 0; ic--)
        {
            if (lptr->m_pieces[ic].m_isarc) 
            {
                aABot->m_laststraight = ic;
                break;
            }
        }
    }
    else {
        _fslip = aABot->Fslip;
    }

    for (ic = 0; ic < lptr->m_numpieces; ic++)
    {
        aABot->m_checkpieces[ic].m_maxvelocity = -1.0;
        if (lptr->m_pieces[ic].m_isarc) 
        {
            int jc;
            double avgrad = 0.0;
            for (jc = 0; jc < aABot->m_race.m_numlanes; jc++) {
                avgrad += aABot->m_race.m_lanes[jc].m_pieces[ic].m_radius;
            }

            avgrad /= (double) aABot->m_race.m_numlanes;

            aABot->m_checkpieces[ic].m_maxvelocity = sqrt(_fslip * avgrad);
        }
    }

    double minvel = -1.0;

    for (ic = lptr->m_numpieces - 1; ic >= 0; ic--)
    {
        if (aABot->m_checkpieces[ic].m_maxvelocity > 0.0)
        {
            if (minvel > 0.0 && minvel < aABot->m_checkpieces[ic].m_maxvelocity)
            {
                aABot->m_checkpieces[ic].m_maxvelocity = minvel;
                minvel = -1.0;
            }
            else if (minvel < 0.0) {
                minvel = aABot->m_checkpieces[ic].m_maxvelocity;
            }
        }
        else {
            minvel = -1.0;
        }
    }

    for (ic = 0; ic < lptr->m_numpieces; ic++)
    {
        if (aABot->m_checkpieces[ic].m_maxvelocity > 0.0)
            fprintf(stdout, "%d: maxv %.8lf\n", ic, aABot->m_checkpieces[ic].m_maxvelocity);
    }
    fflush(stdout);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABots_switchNeeded(ABot_t *aABot, botposition_t *aPos, int aFastest) {

    int ic;
    for (ic = 0; ic < aABot->m_segments.m_numsegments; ic++)
    {
        int res = 0;

        if (aABot->m_segments.m_segments[ic].m_flip == 1)
        {
            if (aPos->m_pieceindex >= aABot->m_segments.m_segments[ic].m_startpiece &&
                aPos->m_pieceindex < aABot->m_race.m_lanes->m_numpieces) {
                res = 1;
            } else if (
                aPos->m_pieceindex >= 0 &&
                aPos->m_pieceindex <= aABot->m_segments.m_segments[ic].m_endpiece) {
                res = 1;
            }
        }
        else if (
            aPos->m_pieceindex >= aABot->m_segments.m_segments[ic].m_startpiece &&
            aPos->m_pieceindex <= aABot->m_segments.m_segments[ic].m_endpiece)
            res = 1;

        if (res)
        {
            int next;
            next = ic + 1;
            int left;
            int right;
            double rlength = 10000;
            double llength = 10000;
            double clength;
            int rc;

            if (next >= aABot->m_segments.m_numsegments) 
                next = 0;

            left = aPos->m_endlaneindex - 1;
            right = aPos->m_endlaneindex + 1;

            clength = aABot->m_segments.m_segments[next].m_lengthperlane[(int)aPos->m_endlaneindex].m_length;

            if (left >= 0) {
                llength = aABot->m_segments.m_segments[next].m_lengthperlane[left].m_length;
                llength += aABot->m_segments.m_segments[next].m_lengthperlane[(int)aPos->m_endlaneindex].m_costl;
            } else {
                if (!aFastest) llength = -1.0;
            }

            if (right < aABot->m_race.m_numlanes) {
                rlength = aABot->m_segments.m_segments[next].m_lengthperlane[right].m_length;
                rlength += aABot->m_segments.m_segments[next].m_lengthperlane[(int)aPos->m_endlaneindex].m_costr;
            } else {
                if (!aFastest) rlength = -1.0;
            }

            if (aFastest == 1) {
                if (clength < llength && clength < rlength) {
                    rc = -1;
                } else if (llength < rlength) {
                    rc = 1;
                } else if (llength > rlength)
                    rc = 0;
            }
            else {
                if (clength > llength && clength > rlength) {
                    rc = -1;
                } else if (llength > rlength) {
                    rc = 1;
                } else if (llength < rlength)
                    rc = 0;
            }

            if (rc != -1)
            {
                aABot->m_nextswitchpiece = aABot->m_segments.m_segments[next].m_startpiece - 1;
                if (aABot->m_segments.m_segments[next].m_startpiece == 0) 
                {
                    aABot->m_nextswitchpiece = aABot->m_race.m_lanes[0].m_numpieces - 1;
                }
            }

#if 0
            fprintf(stdout, "%d: l:%.2lf c:%.2lf r:%.2lf = %d\n", aPos->m_pieceindex, llength, clength, rlength, rc);
#endif
            return rc;
        }

    }
    return -1;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static void ABot_maxthrottle(ABot_t *aABot, int aTick, piece_t *st, botposition_t *current)
{
    if (st->m_isarc)
    {
        // VmaxNoSlip = sqrt(Fslip * R)
        double vmax = sqrt(aABot->Fslip * st->m_radius);
        double vmaxn;
        int nextpiece = current->m_pieceindex;

        vmax = aABot->m_checkpieces[current->m_pieceindex].m_maxvelocity;

        nextpiece++;
        if (nextpiece >= aABot->m_race.m_lanes[0].m_numpieces) {
            nextpiece = 0;
        }
        vmaxn = aABot->m_checkpieces[nextpiece].m_maxvelocity;

        if (vmaxn < 0.0 && (current->m_inpiecedistance / getPieceSize(
                aABot->m_race.m_lanes,
                st, 
                current->m_startlaneindex,
                current->m_endlaneindex) > 0.75))
        {
            if (current->m_anglediff <= 0.0)
                aABot->m_throttle = 1.0;

            return;
        }

        if (current->m_velocity >= vmax) 
            aABot->m_throttle = 0.0;
        else 
        {
            int ic;
            aABot->m_throttle = 1.0;
            for (ic = 0; ic < 100; ic++) {
                double h = ((double)ic)/100.0;
                double vn = calcVelocityNextTick(current->m_velocity, h, aABot->K, aABot->M, 1);
                if (vn >= vmax) {
                    aABot->m_throttle = (h) - 0.01;
                    break;
                }
            }
        }
    }
    else 
    {
        double vmax = -1.0;
        int nextpiece = current->m_pieceindex;
        double distance = 0.0;

        distance = getPieceSize(
                aABot->m_race.m_lanes,
                st, 
                current->m_startlaneindex,
                current->m_endlaneindex);
        do {
            nextpiece++;
            if (nextpiece >= aABot->m_race.m_lanes[0].m_numpieces) {
                nextpiece = 0;
            }
            vmax = aABot->m_checkpieces[nextpiece].m_maxvelocity;
            distance += aABot->m_race.m_lanes[(int)current->m_endlaneindex].m_pieces[nextpiece].m_length;
        } while (vmax < 0);

        aABot->m_throttle = 1.0;
        if (current->m_velocity >= vmax) {
            int kc = 0;
            int lc = 100;
            double nextdistance;
            double nextvelocity;

            // need to check if we can slow down in time
            do {
                do {
                    nextdistance = calcDistanceNextTick(current->m_velocity, lc / 100.0 /* h */, aABot->K, aABot->M, kc, current->m_inpiecedistance);
                    nextvelocity = calcVelocityNextTick(current->m_velocity, lc / 100.0 /* h */, aABot->K, aABot->M, kc);
                    kc++;
                    if (nextvelocity < vmax && (nextdistance + 2) < distance) {
                        break;
                    }
                    kc++;
                } while (kc < 100);

                if (nextvelocity < vmax && (nextdistance + 2) < distance) {
                    break;
                }

                lc--;
            } while (lc > 0);
            aABot->m_throttle = (lc / 100.0);
        }
    }
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
static int ABot_dowork(ABot_t *aABot, int aTick, cJSON *aMsgData, int aIsGameStart)
{
    int ic;
    int doswitch = -1;
    int doturbo = 0;

    if (aMsgData) 
    {
        int size = cJSON_GetArraySize(aMsgData);

        for (ic = 0; ic < size; ic++) 
        {
            cJSON *obj;
            position_t pos = {};
            history_t *his;

            obj = cJSON_GetArrayItem(aMsgData, ic);
            if (obj)
                positionParse(obj, &pos);

            his = historyFind(pos.m_color, aABot->m_history, aABot->m_numcars);
            if (his)
                historyAdd(his, aTick, &pos, aABot->m_race.m_lanes);

            if (aTick == -1)
                fprintf(
                    stdout, 
                    "%cName: '%-16s' Color: '%-16s' %2d %8.4lf\n", 
                    (strcmp(pos.m_color, aABot->m_color) == 0) ? '*' : ' ',
                    pos.m_name, pos.m_color, pos.m_pieceindex, pos.m_inpiecedistance);
        }

        {
            botposition_t *current = aABot->m_myhistory->m_head;
            botposition_t *prev = historyGetPrev(aABot->m_myhistory, aTick);

            piece_t *st = &aABot->m_race.m_lanes[(int)current->m_startlaneindex].m_pieces[(int)current->m_pieceindex];

            double len = 0;
            char buffer[24];

            len = getPieceSize(
                    aABot->m_race.m_lanes,
                    st, 
                    current->m_startlaneindex,
                    current->m_endlaneindex);

            if (current->m_startlaneindex != current->m_endlaneindex)
            {
                snprintf(buffer, sizeof(buffer), "(sw %d=>%d)", current->m_startlaneindex , current->m_endlaneindex);
            }
            else {
                snprintf(buffer, sizeof(buffer), "(lane  %d)", current->m_startlaneindex);
            }

            fprintf(stdout, "%4d: p:%3d in:%8.4lf/%8.4lf (%8.4lf%%) (a: %8.4lf) %s l:%d/%d %c v:%8.4lf a:%8.4lf ad:%8.4lf\n", 
                    aTick, 
                    current->m_pieceindex, 
                    current->m_inpiecedistance, 
                    len,
                    (current->m_inpiecedistance * 100.0/ len), 
                    current->m_angle,
                    buffer,
                    current->m_lap, aABot->m_race.m_session.m_laps,
                    st->m_isarc?'c':' ',
                    current->m_velocity,
                    current->m_acceleration,
                    current->m_anglediff);

            // switched lanes, reset values
            if (prev && prev->m_startlaneindex != current->m_startlaneindex) {
                aABot->m_switchpending = 0;
                aABot->m_nextswitchpiece = -1;
            }

            // failed to switch check
            if (st->m_isswitch && 
                aABot->m_switchpending == 1 &&
                aABot->m_nextswitchpiece == current->m_pieceindex &&
                current->m_startlaneindex == current->m_endlaneindex) {
                aABot->m_switchpending = 0;
                aABot->m_nextswitchpiece = -1;
                fprintf(stdout, "%4d: Failed to switch\n", aTick);
                fflush(stdout);
            }

            if (aTick >= 0 && prev && prev->m_pieceindex != current->m_pieceindex && 
                aABot->m_switchpending == 0 && aABot->Fslip > 0.0) {
                if (aABot->m_switchpending == 0) {
                    doswitch = ABots_switchNeeded(aABot, current, aABot->m_fastest);
                }
            } 

            if (aABot->m_init == 1)
            {
                if (aTick == 3)
                {
                    double v3 = current->m_velocity;
                    double v2 = prev->m_velocity;
                    double v1 = (prev - 1)->m_velocity;
                    double v0 = (prev - 2)->m_velocity;
                    double H = 1.0;

                    // calc
                    fprintf(stdout, "V3: %.8lf\n", v3);
                    fprintf(stdout, "V2: %.8lf\n", v2);
                    fprintf(stdout, "V1: %.8lf\n", v1);
                    fprintf(stdout, "V0: %.8lf\n", v0);
                    aABot->K = ( v1 - ( v2 - v1 ) ) / (v1*v1) * H;
                    aABot->M = 1.0 / ( log( ( v3 - ( H / aABot->K ) ) / ( v2 - ( H / aABot->K ) ) ) / ( -aABot->K ) );
                    fprintf(stdout, " K: %.8lf, M: %.8lf\n", aABot->K, aABot->M);
                    fprintf(stdout, " ?: %.8lf\n", velocity_at_tick(v3, 1.0, aABot->K, aABot->M, 30));
                }
                else if (aTick > 3 && aABot->m_switchpending == 0 && aABot->Fslip > 0.0) {
                    doswitch = ABots_switchNeeded(aABot, current, aABot->m_fastest);
                }

                if (!(aABot->Fslip > 0.0) && 
                    fabs(current->m_angle) > 0.0 && 
                    st->m_isarc &&
                    current->m_startlaneindex == current->m_endlaneindex)
                {
                    double __rad(double angle) { return M_PI * angle / 180.0; }
                    double __deg(double angle) { return angle * 180.0 / M_PI; }
                    double B;
                    double Bslip;
                    double Vthres;

                    B = __deg(current->m_velocity / st->m_radius);
                    Bslip = B - fabs(current->m_angle);
                    Vthres = __rad(Bslip) * st->m_radius;
                    aABot->Fslip = Vthres * Vthres / st->m_radius;
                    fprintf(stdout, "%4d: Fslip %.8lf, Vthres %.8lf\n", aTick, aABot->Fslip, Vthres);
                    if (aABot->Fslip >= 0.40)
                        aABot->Fslip = 0.31;
                    ABot_analyze(aABot, 1);
                }

                if (aTick > 3 && aABot->Fslip > 0.0)
                {
                    ABot_maxthrottle(aABot, aTick, st, current);
                }
            }
            else
            {
                ABot_maxthrottle(aABot, aTick, st, current);
            }

            // enable turbo
            if ((current->m_pieceindex == aABot->m_laststraight && (current->m_inpiecedistance / st->m_length) >= 0.75) ||
                current->m_pieceindex > aABot->m_laststraight)
            {
                if (aABot->m_race.m_session.m_laps > 0 && 
                    current->m_lap + 1 >= aABot->m_race.m_session.m_laps)  {
                    aABot->m_throttle = 1.0;
                    if (aABot->m_turboavailable) {
                        doturbo = 1;
                    }
                }
            }
        }
    }

    if (aTick < 0) {
        return 0;
    }

    if (doswitch != -1) {
        aABot->m_switchpending = 1;
        write_switch_msg(&aABot->m_bot, doswitch);
        fprintf(stdout, "%4d: send switch %s\n", aTick, doswitch != 0 ? "LEFT" : "RIGHT");
        fflush(stdout);
    } else if (doturbo) {
        fprintf(stdout, "%4d: Send Turbo\n", aTick);
        write_turbo_msg(&aABot->m_bot);
    } else {
        write_throttle_msg(&aABot->m_bot, aABot->m_throttle);
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_carpositions(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    return ABot_dowork(aABot, aTick, aMsgData, 0);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_gamestart(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    printf("%4d: Race Started\n", aTick);
    fflush(stdout);

    return ABot_dowork(aABot, aTick, aMsgData, 1);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_gameend(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    printf("%4d: Race ended\n", aTick);
    fflush(stdout);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_lapfinished(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData) {
        cJSON *obj;
        char name[32] = {};
        char color[32] = {};
        int me = 0;

        obj = cJSON_GetObjectItem(aMsgData, "car");
        if (obj)
            extractBotInfo(obj, name, sizeof(name), color, sizeof(color));

        me = !strcmp(color, aABot->m_color);
        printf("%4d: Lap Finished %s'%s' - %s)\n", aTick, me ? "(*":"(", name, color);
        fflush(stdout);

        if (me) {
            // XXX: just flip right now in race, not in qualifications
            if (aABot->m_init == 2) {
                aABot->m_fastest = !aABot->m_fastest;
            }
        }
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_tournamentend(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    puts("Tournament End");

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_join(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    puts("Joined");
    fflush(stdout);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_joinRace(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    puts("joinRace");

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_createRace(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    puts("createRace");
    fflush(stdout);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_yourcar(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    char name[32] = {};

    if (aMsgData) 
        extractBotInfo(
            aMsgData, 
            name, sizeof(name), 
            aABot->m_color, sizeof(aABot->m_color));

    fprintf(stdout, "yourCar color: %s\n", aABot->m_color);
    fflush(stdout);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_error(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData == NULL)
        puts("Unknown error");
    else
        printf("ERROR: %s\n", aMsgData->valuestring);
    fflush(stdout);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_gameinit(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    fprintf(stdout, "%4d: Game Init: %d\n", aTick, aABot->m_init);
    fflush(stdout);

    if (aMsgData) {
        char name[64] = {};

        if (aABot->m_init == 0) {
            parseTrackInit(aMsgData, &aABot->m_race, name, sizeof(name));
            fprintf(stdout, "%4d: Track: %s\n", aTick, name);
            fflush(stdout);
        }
        else
        {
            updateRaceSession(aMsgData, &aABot->m_race);
        }

        if (aABot->m_segments.m_numsegments == 0) {
            makeSegments(&aABot->m_segments, aABot->m_race.m_numlanes, aABot->m_race.m_lanes);

            aABot->m_history = calloc(aABot->m_race.m_numcars, sizeof(aABot->m_history[0]));
            aABot->m_numcars = aABot->m_race.m_numcars;
            if (aABot->m_history) {
                int ic; 
                for (ic = 0; ic < aABot->m_numcars; ic++)
                {
                    historyInit(aABot->m_race.m_cars[ic].m_color, &aABot->m_history[ic]);
                    if (strcmp(aABot->m_race.m_cars[ic].m_color, aABot->m_color) == 0)
                    {
                        aABot->m_myhistory = &aABot->m_history[ic];
                    }
                }
            }
        }

        // analyze track
        ABot_analyze(aABot, 0);

        // TODO: reset stats
        if (aABot->m_history) {
            int ic; 
            for (ic = 0; ic < aABot->m_numcars; ic++)
            {
                aABot->m_history[ic].m_crash = -1;
                aABot->m_history[ic].m_turbo = -1;
            }
        }

        aABot->m_init++;
    }

    return 0;
}
//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
static int ABot_message_crash(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData) {
        char name[32] = {};
        char color[32] = {};
        int me;

        extractBotInfo(aMsgData, name, sizeof(name), color, sizeof(color));
        me = !strcmp(color, aABot->m_color);

        printf("%4d: Someone crashed %s'%s' - %s)\n", aTick, me ? "(*":"(", name, color);
        fflush(stdout);

        history_t *his = historyFind(color, aABot->m_history, aABot->m_numcars);
        if (his) {
            historyAddFlag(his, aTick, 1, 0, 0, 0);
            his->m_crash = aTick;
        }

        if (me)
        {
            aABot->m_turboavailable = 0;
            aABot->m_turbo.m_turbofactor = 1.0;
            aABot->m_turbo_active = 1.0;
            aABot->m_turbo_end = aTick;
        }
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_spawn(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData) {
        char name[32] = {};
        char color[32] = {};
        int me;

        extractBotInfo(aMsgData, name, sizeof(name), color, sizeof(color));

        me = !strcmp(color, aABot->m_color);

        printf("%4d: Someone spawn %s'%s' - %s)\n", aTick, me ? "(*":"(", name, color);
        fflush(stdout);

        history_t *his = historyFind(color, aABot->m_history, aABot->m_numcars);
        if (his) {
            historyAddFlag(his, aTick, 0, 1, 0, 0);
            his->m_crash = -1;
        }
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_dnf(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData) {
        char name[32] = {};
        char color[32] = {};
        int me;

        extractBotInfo(aMsgData, name, sizeof(name), color, sizeof(color));

        me = !strcmp(color, aABot->m_color);

        printf("%4d: Someone dnf %s'%s' - %s)\n", aTick, me ? "(*":"(", name, color);
        fflush(stdout);

        history_t *his = historyFind(color, aABot->m_history, aABot->m_numcars);
        if (his) {
            his->m_dnf = 1;
        }
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_turbostart(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData) {
        char name[32] = {};
        char color[32] = {};
        int me;

        extractBotInfo(aMsgData, name, sizeof(name), color, sizeof(color));
        me = !strcmp(color, aABot->m_color);

        printf("%4d: Someone started turbo %s'%s' - %s)\n", aTick, me ? "(*":"(", name, color);
        fflush(stdout);

        history_t *his = historyFind(color, aABot->m_history, aABot->m_numcars);
        if (his) {
            historyAddFlag(his, aTick, 0, 0, 1, 0);
            his->m_turbo = aTick;
        }

        if (me)
        {
            aABot->m_turboavailable = 0;
            aABot->m_turbo_active = aABot->m_turbo.m_turbofactor;
            aABot->m_turbo_end = aTick + aABot->m_turbo.m_turbodurationticks;
            aABot->m_turbo.m_turbofactor = 1.0;
        }
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_turboend(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData) {
        char name[32] = {};
        char color[32] = {};
        int me;
        
        extractBotInfo(aMsgData, name, sizeof(name), color, sizeof(color));
        me = !strcmp(color, aABot->m_color);

        printf("%4d: Someone end turbo %s'%s' - %s)\n", aTick, me ? "(*":"(", name, color);
        fflush(stdout);

        history_t *his = historyFind(color, aABot->m_history, aABot->m_numcars);
        if (his) {
            historyAddFlag(his, aTick, 0, 0, 0, 1);
            his->m_turbo = -1;
        }

        if (me)
        {
            aABot->m_turbo_active = 1.0;
            aABot->m_turbo_end = aTick;
            aABot->m_turbo.m_turbofactor = 1.0;
        }
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_finish(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData) {
        char name[32] = {};
        char color[32] = {};
        int me;
        
        extractBotInfo(aMsgData, name, sizeof(name), color, sizeof(color));
        me = !strcmp(color, aABot->m_color);

        printf("%4d: Someone finish %s'%s' - %s)\n", aTick, me ? "(*":"(", name, color);
        fflush(stdout);
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static int ABot_message_turboavailable(ABot_t *aABot, int aTick, cJSON *aMsgData)
{
    if (aMsgData) {
        aABot->m_turboavailable = 1;

        turboParse(aMsgData, &aABot->m_turbo);

        printf("%4d: turbo factor: %lf ticks: %d msecs %lf\n", 
            aTick,
            aABot->m_turbo.m_turbofactor,
            aABot->m_turbo.m_turbodurationticks,
            aABot->m_turbo.m_turbodurationmilliseconds);
        fflush(stdout);
    }

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
int ABot_process_msg(struct _bot *aBot, cJSON *aMsg)
{
    ABot_t *abot = (ABot_t *) aBot;

    cJSON *msg_type;
    char *msg_type_name;
    cJSON *msg_data;
    cJSON *obj;
    int tick = -1;

    int ic;
    struct {
        char *msg;
        int (*msgfunc)(ABot_t *aABot, int aTick, cJSON *aMsgData);
    } msgs[] = {
        { "yourCar", &ABot_message_yourcar },
        { "gameEnd", &ABot_message_gameend },
        { "gameStart", &ABot_message_gamestart },
        { "join", &ABot_message_join },
        { "joinRace", &ABot_message_joinRace },
        { "createRace", &ABot_message_createRace },
        { "carPositions", &ABot_message_carpositions },
        { "turboAvailable", &ABot_message_turboavailable },
        { "gameInit", &ABot_message_gameinit },
        { "error", &ABot_message_error },
        { "crash", &ABot_message_crash },
        { "spawn", &ABot_message_spawn },
        { "dnf", &ABot_message_dnf },
        { "turboStart", &ABot_message_turbostart },
        { "turboEnd", &ABot_message_turboend },
        { "finish", &ABot_message_finish },
        { "lapFinished", &ABot_message_lapfinished },
        { "tournamentEnd", &ABot_message_tournamentend },
    };

    msg_type = cJSON_GetObjectItem(aMsg, "msgType");
    if (!msg_type) 
        return -1;

    obj = cJSON_GetObjectItem(aMsg, "gameTick");
    if (obj) 
        tick = obj->valueint;

    msg_type_name = msg_type->valuestring;

    msg_data = cJSON_GetObjectItem(aMsg, "data");

    for (ic = 0; ic < sizeof(msgs)/sizeof(msgs[0]); ic++) {
        if (strcmp(msgs[ic].msg, msg_type_name) == 0) {
            int rc;
            rc =  msgs[ic].msgfunc(abot, tick, msg_data);
            return rc;
        }
    }

    fprintf(stdout, "Unknown %s\n", msg_type_name);
    fflush(stdout);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
int ABot_wlog(struct _bot *aBot, const char *aBuffer, int aSize)
{
#if 0
    //ABot_t *abot = (ABot_t *) aBot;

    //if (!abot->m_silent)
        fprintf(stdout, "w[%s]:%s", aBot->name, aBuffer);
    fflush(stdout);
#endif
    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
int ABot_rlog(struct _bot *aBot, const char *aBuffer, int aSize)
{
#if 0
    //ABot_t *abot = (ABot_t *) aBot;

    //if (!abot->m_silent)
        fprintf(stdout, "r[%s]:%s\n", aBot->name, aBuffer);
    fflush(stdout);

#endif

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
int ABot_close(struct _bot *aBot)
{
    ABot_t *_bot = (ABot_t *) aBot;

    if(_bot->m_history) {
       free(_bot->m_history);
       _bot->m_history = NULL;
    }
    freeSegments(&_bot->m_segments);
    freeRace(&_bot->m_race);
    free(_bot);

    return 0;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
void ABotInit(bot_t **aBot, int fd, const char *aName, const char *aKey, int aSilent)
{
    ABot_t *abot;

    abot = calloc(1, sizeof(*abot));

    *aBot = &abot->m_bot;

    abot->m_silent = aSilent;
    abot->m_bot.fd = fd;

    strncpy(abot->m_bot.name, aName, sizeof(abot->m_bot.name));
    strncpy(abot->m_bot.key, aKey, sizeof(abot->m_bot.key));

    abot->m_bot.process_msg = &ABot_process_msg;
    abot->m_bot.rlog = &ABot_rlog;
    abot->m_bot.wlog = &ABot_wlog;
    abot->m_bot.close = &ABot_close;
    abot->m_throttle = 1.0;
    abot->m_fastest = 1;
    abot->Fslip = 0.0;
}
//------------------------------------------------------------------------------

#if 0
char str_crash[] = 
#include "crash.h"
//#include "a.h"
;

int main(int argc, char *argv[])
{
    cJSON *json;
    ABot_t bot = {};

    json = cJSON_Parse(str_crash);
    if (json) {
        cJSON *data;

        data = cJSON_GetObjectItem(json, "data");
        ABot_message_gameinit(&bot, -1, data);
        cJSON_Delete(json);
    }


    return 0;
}
#endif
