#ifndef __POSITION_H_INC__
#define __POSITION_H_INC__

#include "cJSON.h"

typedef struct {
    char           m_name[17];
    char           m_color[17];
    double         m_angle;
    int            m_pieceindex;
    double         m_inpiecedistance;
    char           m_startlaneindex;
    char           m_endlaneindex;
    char           m_lap;

} position_t;

typedef struct {
    int    m_tick;
    int    m_pieceindex;
    double m_inpiecedistance;
    double m_angle;
    char   m_startlaneindex;
    char   m_endlaneindex;
    char   m_lap;

    double         m_velocity;
    double         m_acceleration;
    double         m_anglediff;

    char   m_crash:1;
    char   m_spawn:1;
    char   m_turbostart:1;
    char   m_turboend:1;
} botposition_t;

extern int positionParse(cJSON *aData, position_t *aPosition);

#endif /* __POSITION_H_INC__ */
