#ifndef __CURVES_H_INC__
#define __CURVES_H_INC__

extern double bendCrossCurveLength(
        double fromLaneDistance,
        double toLaneDistance,
        double radius,
        double angle);

#endif /* __CURVES_H_INC__ */
