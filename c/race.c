#include "race.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

static int processRaceSession(cJSON *aData, race_t *aRace)
{
    cJSON *obj;

    aRace->m_session.m_laps = 0;
    aRace->m_session.m_maxlaptimems = 0;
    aRace->m_session.m_quickrace = 0;
    aRace->m_session.m_durationms = 0;
    obj = cJSON_GetObjectItem(aData, "laps");
    if (obj) {
        aRace->m_session.m_laps = obj->valueint;
        fprintf(stdout, "LAPS: %d\n", obj->valueint);
    }


    obj = cJSON_GetObjectItem(aData, "maxLapTimeMs");
    if (obj) {
        aRace->m_session.m_maxlaptimems = obj->valueint;
        fprintf(stdout, "MAXLAPTIMEMS: %d\n", obj->valueint);
    }

    obj = cJSON_GetObjectItem(aData, "quickRace");
    if (obj) { 
        if (obj->type == cJSON_True)
            aRace->m_session.m_quickrace = 1;
        fprintf(stdout, "QUICKRACE: %d\n", aRace->m_session.m_quickrace);
    }

    obj = cJSON_GetObjectItem(aData, "durationMs");
    if (obj) {
        aRace->m_session.m_durationms = obj->valueint;
        fprintf(stdout, "DURATIONMS: %d\n", obj->valueint);
    }

    return 0;
}

static int processCars(cJSON *aData, race_t *aRace)
{
    int ic;
    car_t *ptr;

    aRace->m_numcars = cJSON_GetArraySize(aData);
    aRace->m_cars = calloc(aRace->m_numcars, sizeof(*aRace->m_cars));
    if (!aRace->m_cars) return -1;

    for (ic = 0; ic < aRace->m_numcars; ic++) {
        cJSON *car;
        cJSON *obj;
        cJSON *obj2;

        car = cJSON_GetArrayItem(aData, ic);
        if (!car) return -1;

        ptr = &aRace->m_cars[ic];

        {
            obj = cJSON_GetObjectItem(car, "dimensions");
            if (!obj) return -1;

            obj2 = cJSON_GetObjectItem(obj, "guideFlagPosition");
            if (!obj2) return -1;
            ptr->m_guideflagposition = obj2->valuedouble;

            obj2 = cJSON_GetObjectItem(obj, "length");
            if (!obj2) return -1;
            ptr->m_length = obj2->valuedouble;

            obj2 = cJSON_GetObjectItem(obj, "width");
            if (!obj2) return -1;
            ptr->m_width = obj2->valuedouble;
        }

        {
            obj = cJSON_GetObjectItem(car, "id");
            if (!obj) return -1;

            obj2 = cJSON_GetObjectItem(obj, "color");
            if (!obj2) return -1;
            strncpy(ptr->m_color, obj2->valuestring, sizeof(ptr->m_color));

            obj2 = cJSON_GetObjectItem(obj, "name");
            if (!obj2) return -1;
            strncpy(ptr->m_name, obj2->valuestring, sizeof(ptr->m_name));
        }
    }

    return 0;
}

static double  __calcarclength(double aAngle, double aRadius)
{
    return M_PI * fabs(aAngle) * aRadius / 180.0;
}

static int processTrack(cJSON *aData, race_t *aRace, char *aID, int aIDsz)
{
    int ic;
    cJSON *obj;
    cJSON *lanesobj;
    cJSON *piecesobj;
    piece_t *temppieces;
    int num_pieces;

    obj = cJSON_GetObjectItem(aData, "id");
    if (obj) strncpy(aID, obj->valuestring, aIDsz);

    lanesobj = cJSON_GetObjectItem(aData, "lanes");
    if (!lanesobj) return -1;

    piecesobj = cJSON_GetObjectItem(aData, "pieces");
    if (!piecesobj) return -1;

    aRace->m_numlanes = cJSON_GetArraySize(lanesobj);
    aRace->m_lanes = calloc(aRace->m_numlanes, sizeof(*aRace->m_lanes));
    if (!aRace->m_lanes) return -1;

    for (ic = 0; ic < aRace->m_numlanes; ic++) {
        cJSON *laneobj;
        cJSON *obj;

        laneobj = cJSON_GetArrayItem(lanesobj, ic);
        if (!laneobj) return -1;

        obj = cJSON_GetObjectItem(laneobj, "distanceFromCenter");
        if (!obj) return -1;
        aRace->m_lanes[ic].m_distancefromcenter = obj->valuedouble;

        obj = cJSON_GetObjectItem(laneobj, "index");
        if (!obj) return -1;
        aRace->m_lanes[ic].m_index = obj->valueint;
    }

    num_pieces = cJSON_GetArraySize(piecesobj);
    temppieces = calloc(num_pieces, sizeof(*temppieces));
    if (!temppieces) return -1;

    for (ic = 0; ic < num_pieces; ic++)
    {
        cJSON *pieceobj;
        cJSON *obj;
        piece_t *piece_ptr;

        piece_ptr = &temppieces[ic];
        pieceobj = cJSON_GetArrayItem(piecesobj, ic);

        obj = cJSON_GetObjectItem(pieceobj, "length");
        if (obj)
            piece_ptr->m_length = obj->valuedouble;

        obj = cJSON_GetObjectItem(pieceobj, "radius");
        if (obj) {
            piece_ptr->m_isarc = 1;
            piece_ptr->m_radius = obj->valuedouble;
        }

        obj = cJSON_GetObjectItem(pieceobj, "angle");
        if (obj)
            piece_ptr->m_angle = obj->valuedouble;

        obj = cJSON_GetObjectItem(pieceobj, "switch");
        if (obj && obj->type == cJSON_True)
            piece_ptr->m_isswitch = 1;

        obj = cJSON_GetObjectItem(pieceobj, "bridge");
        if (obj && obj->type == cJSON_True)
            piece_ptr->m_isbridge = 1;
    }

    fprintf(stdout, "lanes(%d): ", aRace->m_numlanes);
    for (ic = 0; ic < aRace->m_numlanes; ic++) {
        fprintf(stdout, " (%d)%6.2lf", ic, aRace->m_lanes[ic].m_distancefromcenter);
    }
    fprintf(stdout, "\n");

    for (ic = 0; ic < num_pieces; ic++) {
        piece_t *piece_ptr;

        piece_ptr = &temppieces[ic];

        if (piece_ptr->m_isarc) {
            fprintf(stdout, "%3d: s: %d r: %6.2lf a: %6.2lf\n", ic, piece_ptr->m_isswitch, piece_ptr->m_radius, piece_ptr->m_angle);
        } else {
            fprintf(stdout, "%3d: s: %d l: %6.2lf\n", ic, piece_ptr->m_isswitch, piece_ptr->m_length);
        }
    }

    for (ic = 0; ic < aRace->m_numlanes; ic++)
    {
        int jc;
        lane_t *ptr = &aRace->m_lanes[ic];


        ptr->m_numpieces = num_pieces;
        ptr->m_pieces = calloc(ptr->m_numpieces, sizeof(*ptr->m_pieces));
        if (!ptr->m_pieces) return -1;

        for (jc = 0; jc < ptr->m_numpieces; jc++) {
            piece_t *piece_ptr;

            piece_ptr = &ptr->m_pieces[jc];

            *piece_ptr = temppieces[jc];
            piece_ptr->m_lane = ic;
            if (piece_ptr->m_isarc) {
                if (piece_ptr->m_angle < 0)
                    piece_ptr->m_radius += ptr->m_distancefromcenter;
                else
                    piece_ptr->m_radius -= ptr->m_distancefromcenter;
                piece_ptr->m_length = __calcarclength(piece_ptr->m_angle, piece_ptr->m_radius);
#if 0
                fprintf(stdout, "%3d(%d): l: %6.2lf r: %6.2lf\n", jc, ic, piece_ptr->m_length, piece_ptr->m_radius);
#endif
            }
            else {
#if 0
                fprintf(stdout, "%3d(%d): l: %6.2lf\n", jc, ic, piece_ptr->m_length);
#endif
            }
        }
    }

    free(temppieces);

    return 0;
}

int parseTrackInit(cJSON *aData, race_t *aRace, char *aID, int aIDsz)
{
    cJSON *race;
    cJSON *cars;
    cJSON *track;
    cJSON *session;
    int rc;
    
    memset(aRace, 0, sizeof(*aRace));

    if (!aData) return -1;

    race = cJSON_GetObjectItem(aData, "race");
    if (!race) return -1;

    track = cJSON_GetObjectItem(race, "track");
    if (!track) return -1;

    cars = cJSON_GetObjectItem(race, "cars");
    if (!cars) return -1;

    session = cJSON_GetObjectItem(race, "raceSession");
    if (!session) return -1;

    rc = processTrack(track, aRace, aID, aIDsz);

    rc = processCars(cars, aRace);
    if (rc != 0) return rc;

    processRaceSession(session, aRace);

    return rc;
}

int updateRaceSession(cJSON *aData, race_t *aRace)
{
    cJSON *race;
    cJSON *session;
    
    if (!aData) return -1;

    race = cJSON_GetObjectItem(aData, "race");
    if (!race) return -1;

    session = cJSON_GetObjectItem(race, "raceSession");
    if (!session) return -1;

    processRaceSession(session, aRace);

    return 0;
}

static void freeCars(race_t *aRace)
{
    if (aRace && aRace->m_cars) {
        free(aRace->m_cars);
        aRace->m_cars = NULL;
    }
}

static void freeLanes(lane_t *aLanes, int aNumLanes)
{
    int ic;

    for (ic = 0; ic < aNumLanes; ic++) {
        if (aLanes[ic].m_pieces) {
            free(aLanes[ic].m_pieces);
            aLanes[ic].m_pieces = NULL;
        }
    }
}

static void freeTrack(race_t *aRace)
{
    if (aRace && aRace->m_lanes) {
        freeLanes(aRace->m_lanes, aRace->m_numlanes);
        free(aRace->m_lanes);
        aRace->m_lanes = NULL;
    }
}

void freeRace(race_t *aRace)
{
    freeCars(aRace);
    freeTrack(aRace);
}

#if 0
char str_keimola[] = 
#include "a.h"
;
char str_germany[] = 
#include "b.h"
;
#endif
#if 0
char str_crash[] = 
#include "crash.h"
;
#endif

#if 0
int main(int argc, char *argv[])
{
    cJSON *json;
    race_t race;

    json = cJSON_Parse(str_crash);
    if (json) {
        char name[32];
        cJSON *data;

        data = cJSON_GetObjectItem(json, "data");
        parseTrackInit(data, &race, name, sizeof(name));
        freeRace(&race);
        cJSON_Delete(json);
    }

#if 0
    json = cJSON_Parse(str_germany);

    if (json)
        cJSON_Delete(json);
#endif

    return 0;
}
#endif
