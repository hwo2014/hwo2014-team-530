#include "position.h"

#include <string.h>

int positionParse(cJSON *aData, position_t *aPosition)
{
    cJSON *obj;
    cJSON *obj2;
    obj = cJSON_GetObjectItem(aData, "id");
    if (!obj) return -1;

    obj2 = cJSON_GetObjectItem(obj, "name");
    if (!obj2) return -1;
    strncpy(aPosition->m_name, obj2->valuestring, sizeof(aPosition->m_name));
    obj2 = cJSON_GetObjectItem(obj, "color");
    if (!obj2) return -1;
    strncpy(aPosition->m_color, obj2->valuestring, sizeof(aPosition->m_color));

    obj = cJSON_GetObjectItem(aData, "piecePosition");
    if (!obj) return -1;

    obj2 = cJSON_GetObjectItem(obj, "pieceIndex");
    if (!obj2) return -1;
    aPosition->m_pieceindex = obj2->valueint;

    obj2 = cJSON_GetObjectItem(obj, "inPieceDistance");
    if (!obj2) return -1;
    aPosition->m_inpiecedistance = obj2->valuedouble;

    obj2 = cJSON_GetObjectItem(obj, "lap");
    if (!obj2) return -1;
    aPosition->m_lap = obj2->valueint;

    obj = cJSON_GetObjectItem(obj, "lane");
    if (!obj) return -1;
    obj2 = cJSON_GetObjectItem(obj, "startLaneIndex");
    if (!obj2) return -1;
    aPosition->m_startlaneindex = obj2->valueint;

    obj2 = cJSON_GetObjectItem(obj, "endLaneIndex");
    if (!obj2) return -1;
    aPosition->m_endlaneindex = obj2->valueint;

    obj = cJSON_GetObjectItem(aData, "angle");
    if (!obj) return -1;
    aPosition->m_angle = obj->valuedouble;

    return 0;
}

#if 0
r:{"msgType":"carPositions","data":[{"id":{"name":"Canuck Turbo","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"c35d6507-f675-4bab-8014-29b7304e3407"}

r:{"msgType":"carPositions","data":[{"id":{"name":"Canuck Turbo","color":"red"},"angle":0.0,"piecePosition":{"pieceIndex":0,"inPieceDistance":0.0,"lane":{"startLaneIndex":0,"endLaneIndex":0},"lap":0}}],"gameId":"c35d6507-f675-4bab-8014-29b7304e3407","gameTick":1}
#endif
