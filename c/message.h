#ifndef __MESSAGE_H_INC__
#define __MESSAGE_H_INC__

#include "bot.h"

extern int write_join_msg(bot_t *aBot);
extern int write_createrace_msg(bot_t *aBot, char *trackname, char *password, int carcount);
extern int write_joinrace_msg(bot_t *aBot, char *trackname, char *password, int carcount);

extern int write_ping_msg(bot_t *aBot);
extern int write_throttle_msg(bot_t *aBot, double throttle);
extern int write_switch_msg(bot_t *aBot, int left);
extern int write_turbo_msg(bot_t *aBot);

#endif /* __MESSAGE_H_INC__ */
