#ifndef __SEGMENTS_H_INC__
#define __SEGMENTS_H_INC__

#include "race.h"

typedef struct {
    double m_costl;
    double m_costr;
    double m_length;
} lanelength_t;

typedef struct
{
    int           m_flip;
    int           m_startpiece;
    int           m_endpiece;
    lanelength_t* m_lengthperlane;
    double        m_costl;
    double        m_costr;
} segment_t;

typedef struct
{
    int        m_numsegments;
    int        m_numlanes;
    segment_t* m_segments;
} segments_t;

int makeSegments(segments_t *aSegments, int aNumLanes, lane_t *aLanes);
int freeSegments(segments_t *aSegments);

#endif /* __SEGMENTS_H_INC__ */
