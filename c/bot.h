#ifndef __BOT_H_INC__
#define __BOT_H_INC__

#include "cJSON.h"

typedef struct _bot
{
    int is_ci;
    int fd;
    int idx;
    char name[32];
    char key[32];
    int (*process_msg)(struct _bot *, cJSON *);
    int (*wlog)(struct _bot *, const char *aBuffer, int aSize);
    int (*rlog)(struct _bot *, const char *aBuffer, int aSize);
    int (*close)(struct _bot *);

    int pending_size;
    char *pending;
} bot_t;
#endif /* __BOT_H_INC__ */
