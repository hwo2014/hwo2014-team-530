#include "curves.h"

#include <stdio.h>
#include <math.h>

typedef struct {
    double x;
    double y;
} point_t;

static double qblen(point_t * p0, point_t * p1, point_t* p2)
{
    point_t a;
    point_t b;

    a.x = p0->x - 2.0*p1->x + p2->x;
    a.y = p0->y - 2.0*p1->y + p2->y;
    b.x = 2.0*p1->x - 2.0*p0->x;
    b.y = 2.0*p1->y - 2.0*p0->y;

    double A = 4.0*(a.x*a.x + a.y*a.y);
    double B = 4.0*(a.x*b.x + a.y*b.y);
    double C = b.x*b.x + b.y*b.y;

    double Sabc = 2.0*sqrt(A+B+C);
    double A_2 = sqrt(A);
    double A_32 = 2.0*A*A_2;
    double C_2 = 2.0*sqrt(C);
    double BA = B/A_2;

    return ( A_32*Sabc + A_2*B*(Sabc-C_2) + (4.0*C*A-B*B)*log( (2.0*A_2+BA+Sabc)/(BA+C_2) ) )/(4.0*A_32);
}

static double rad(double angle) { return M_PI * angle / 180.0; }

double bendCrossCurveLength(
        double fromLaneDistance,
        double toLaneDistance,
        double radius,
        double angle)
{
    point_t p0;
    point_t p1;
    point_t p2;
    double mirror = (angle > 0) ? -1.0 : 1.0;
    double len = 0;


    if (fromLaneDistance == -10.0 && 
        toLaneDistance == 10.0 &&
        radius == 200.0 &&
        fabs(angle) == 22.50)
        len = 81.03;

    if (fromLaneDistance == 10.0 && 
        toLaneDistance == -10.0 &&
        radius == 100.0 &&
        fabs(angle) == 45)
        len = 81.02;

    if (fromLaneDistance == 10.0 && 
        toLaneDistance == -10.0 &&
        radius == 200.0 &&
        fabs(angle) == 22.5)
        len = 81.049883;

    if (fromLaneDistance == -10.0 && 
        toLaneDistance == 10.0 &&
        radius == 100.0 &&
        fabs(angle) == 45.0)
        len = 80.7913 + 0.2257;

    if (fromLaneDistance == -20.0 &&
        toLaneDistance == 0 &&
        radius == 100.0 &&
        fabs(angle) == 45.0)
        len = 71.856626 + 1.5097 + 0.0820;

    if (len == 0) {
        p0.x = mirror * (radius + fromLaneDistance);
        p0.y = 0;
        p2.x = (toLaneDistance + radius) * cos(rad(angle)) * mirror;
        p2.y = (toLaneDistance + radius) * sin(rad(angle)) * mirror;
        p1.x = ((toLaneDistance + fromLaneDistance) / 2.0 + radius) * cos(rad(angle) / 2.0) * mirror;
        p1.y = ((toLaneDistance + fromLaneDistance) / 2.0 + radius) * sin(rad(angle) / 2.0) * mirror;

        len = qblen(&p0, &p1, &p2);

        fprintf(stdout, "%s:%d %lF %lf %lf %lf = %lf\n", __FILE__, __LINE__, fromLaneDistance, toLaneDistance, radius, angle, len);
    }

    return len;
}
