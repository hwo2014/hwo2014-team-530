#ifndef __ABOT_H_INC__
#define __ABOT_H_INC__

#include "bot.h"
extern void ABotInit(bot_t **aBot, int fd, const char *aName, const char *aKey, int silent);

#endif /* __ABOT_H_INC__ */
