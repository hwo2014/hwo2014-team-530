#include "history.h"

#include "calc.h"

#include <string.h>
#include <stdlib.h>
#include <math.h>

//------------------------------------------------------------------------------
static double __calcVelocity(botposition_t *aCurrent, botposition_t *aPrev, lane_t *aLanes)
{
    double length = 0;
    double ticks = 1;

    ticks = aCurrent->m_tick - aPrev->m_tick;

    if (aPrev->m_tick < 0)
        ticks = aCurrent->m_tick;

    if (ticks < 0) 
        ticks = 1;

    if (aCurrent->m_pieceindex == aPrev->m_pieceindex) {
        length = aCurrent->m_inpiecedistance - aPrev->m_inpiecedistance;
    }
    else {
        piece_t *st = &aLanes[(int)aPrev->m_startlaneindex].m_pieces[(int)aPrev->m_pieceindex];

        double len = getPieceSize(aLanes, st, aPrev->m_startlaneindex, aPrev->m_endlaneindex);
        if (aPrev->m_inpiecedistance >= len) {
            length = 0;
        }
        else {
             length = len - aPrev->m_inpiecedistance;
        }

        if (aPrev->m_pieceindex + 1 >= aLanes[(int)aPrev->m_endlaneindex].m_numpieces &&
            aCurrent->m_pieceindex >= 0)
        {
            if (aCurrent->m_pieceindex > 0)
            {
                int ic;

                for (ic = 0; ic < aCurrent->m_pieceindex - 1; ic++)
                {
                    st = &aLanes[(int)aCurrent->m_startlaneindex].m_pieces[ic];

                    length += getPieceSize(aLanes, st, aCurrent->m_startlaneindex, aCurrent->m_startlaneindex);
                }
            }
        }
        else {
            if (aPrev->m_pieceindex + 1 < aLanes[(int)aPrev->m_endlaneindex].m_numpieces && aCurrent->m_pieceindex > 0)
            {
                int ic;
                for (ic = aPrev->m_pieceindex + 1; ic < aCurrent->m_pieceindex - 1; ic++)
                {
                    st = &aLanes[(int)aCurrent->m_startlaneindex].m_pieces[ic];

                    length += getPieceSize(aLanes, st, aCurrent->m_startlaneindex, aCurrent->m_startlaneindex);
                }
            }
        }

        length += aCurrent->m_inpiecedistance;
    }

    return length / ticks;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static double calcCurrentVelocity(history_t *aHistory, lane_t *aLanes)
{
    botposition_t *current = aHistory->m_head;
    botposition_t *prev    = historyGetPrev(aHistory, current->m_tick);

    if (!prev || !current) 
        return 0.0;

    return __calcVelocity(current, prev, aLanes);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static double calcCurrentAngleDiff(history_t *aHistory)
{
    botposition_t *current = aHistory->m_head;
    botposition_t *prev    = historyGetPrev(aHistory, current->m_tick);
    double val;

    if (!prev || !current) 
        return 0.0;

    if (current->m_angle < 0.0 && prev->m_angle < 0.0) {
        return current->m_angle - prev->m_angle;
    }

    if (current->m_angle > 0.0 && prev->m_angle > 0.0) {
        return current->m_angle - prev->m_angle;
    }

    val = fabs(prev->m_angle) + fabs(current->m_angle);
    if (prev->m_angle > 0.0)
        return - val;

    return val;
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
static double calcCurrentAcceleration(history_t *aHistory, lane_t *aLanes)
{
    botposition_t *current = aHistory->m_head;
    botposition_t *prev    = historyGetPrev(aHistory, current->m_tick);
    botposition_t *prev2   = historyGetPrev(aHistory, current->m_tick - 1);
    double vel1;
    double vel2;

    if (!prev || !current || !prev2) 
        return 0.0;

    vel1 = __calcVelocity(prev, prev2, aLanes);
    vel2 = __calcVelocity(current, prev, aLanes);

    return vel2 - vel1;
}
//------------------------------------------------------------------------------

void historyInit(char *aColor, history_t *aHistory)
{
    memset(aHistory, 0, sizeof(*aHistory));

    aHistory->m_head = NULL;
    aHistory->m_tail = NULL;
    aHistory->m_end = &aHistory->m_positions[MAX_POSITIONS-1];
    aHistory->m_start = &aHistory->m_positions[0];

    strncpy(aHistory->m_color, aColor, sizeof(aHistory->m_color));
}

botposition_t *historyGetNext(history_t *aHistory, int aTick)
{
    botposition_t *pos;

    pos = aHistory->m_head;

    if (pos == NULL || pos->m_tick != aTick) {
        if (aHistory->m_head == NULL ||
            aHistory->m_head == aHistory->m_end) 
        {
            pos = aHistory->m_start;
            if (aHistory->m_tail)
                aHistory->m_tail++;
        }
        else
        {
            pos = aHistory->m_head + 1;
        }

        aHistory->m_head = pos;

        if (aHistory->m_tail == NULL ||
            aHistory->m_tail == aHistory->m_end) 
        {
            aHistory->m_tail = aHistory->m_start;
        }
    }

    return pos;
}

botposition_t *historyGetPrev(history_t *aHistory, int aTick)
{
    botposition_t *pos;
    botposition_t *end;

    if (aHistory->m_head->m_tick == aTick) {
        if (aHistory->m_head == aHistory->m_start)
        {
            return aHistory->m_end;
        }

        return (aHistory->m_head - 1);
    }

    end = aHistory->m_end;
    if (aHistory->m_tail && aHistory->m_tail < aHistory->m_head)
        end = aHistory->m_tail;

    pos = aHistory->m_head;
    while (pos >= end)
    {
        if (pos->m_tick == aTick) {
            if (pos == end) {
                return NULL;
            }
            return pos - 1;
        }
        pos--;
    }

    return NULL;
}

void historyAddFlag(history_t *aHistory, int aTick, int aCrash, int aSpawn, int aTurboStart, int aTurboEnd)
{
    botposition_t *pos = historyGetNext(aHistory, aTick);

    if (aCrash)      pos->m_crash = 1;
    if (aSpawn)      pos->m_spawn = 1;
    if (aTurboStart) pos->m_turbostart = 1;
    if (aTurboEnd)   pos->m_turboend = 1;
}

void historyAdd(history_t *aHistory, int aTick, position_t *aPosition, lane_t *aLanes) 
{
    botposition_t *pos = historyGetNext(aHistory, aTick);

    pos->m_tick            = aTick;
    pos->m_pieceindex      = aPosition->m_pieceindex;
    pos->m_inpiecedistance = aPosition->m_inpiecedistance;
    pos->m_startlaneindex  = aPosition->m_startlaneindex;
    pos->m_endlaneindex    = aPosition->m_endlaneindex;
    pos->m_lap             = aPosition->m_lap;
    pos->m_angle           = aPosition->m_angle;
    pos->m_velocity        = calcCurrentVelocity(aHistory, aLanes);
    pos->m_acceleration    = calcCurrentAcceleration(aHistory, aLanes);
    pos->m_anglediff       = calcCurrentAngleDiff(aHistory);
}

history_t *historyFind(const char *aColor, history_t *aHistory, int aCount)
{
    int ic;
    history_t *his = NULL;

    if (!aHistory)
        return NULL;

    for (ic = 0; ic < aCount; ic++)
    {
        if (strcmp(aHistory[ic].m_color, aColor) == 0)
        {
            his = &aHistory[ic];
            break;
        }
    }

    return his;
}
